/**
 * Created by Alessio on 20/02/2017.
 */
ngapp.directive("navbar", function ($location, State) {
    return {
        restrict: 'E',
        templateUrl: "partials/navbar.html",
        controllerAs: "navCtrl",
        controller: function () {

            this.isSelected = function (checkTab) {
                return $location.path() === checkTab;
            };
            this.setTab = function (setTab) {
                $(".navbar-collapse").collapse("hide");
                State.navBarParams.tabIndex = setTab;
                // State.navBarParams.currentTitle = State.navBarParams.navTitles[setTab];
            };
            this.currentTitle = function(){
                return State.navBarParams.navTitles[$location.path()];
            };
            this.showMenu = function(){
                return $location.path() === "/cityDetails"
            };
            // $rootScope.$on('$routeChangeStart', function (event, next, current) {
            //
            //     event.currentScope.navCtrl.setTab(tab);
            // });
        },
        link: function(scope, element, attrs, ctrl){

        }
    };
});
ngapp.directive("datetimepicker", function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
            element.datetimepicker({
                locale: 'it',
                inline: true,
                sideBySide: true
            });
            ngModelCtrl.$setViewValue(element.data("DateTimePicker").date());
            element.bind("dp.change", function (e) {
                ngModelCtrl.$setViewValue(e.date, 'change')
            });

        }
    }
});
ngapp.directive("cityDetail", function () {
    return {
        restrict: 'E',
        templateUrl: "partials/cityDetails.html",
        controller: "CityDetailsController",
        controllerAs: "detailsCtrl",
        // link: function(scope, element, attrs){
        //     element.affix({//da far partire quando la view viene caricata
        //         offset: {
        //             top: element.offset().top
        //         }
        //     });
        // },
        bindToController: true
    };


});
ngapp.directive("clock", function () {
    return {
        restrict: 'E',
        templateUrl: "partials/clock.html",
        controllerAs: "clockCtrl",
        scope: {
            timezone: "=",
        },
        controller: "ClockController",
        // link : function (scope, element, attrs) {
        //     scope.$watch("timezone", function (a) {
        //         console.log("changed with watch")
        //     });
        //     attrs.$observe("timezone", function(a){console.log("changed with osberve")});
        // }
    }
});
ngapp.directive("date", function () {
    return {
        restrict: 'E',
        templateUrl: "partials/date.html",
        controllerAs: "clockCtrl",
        scope: {
            timezone: "=timezone"
        },
        controller: "ClockController"
    }
});