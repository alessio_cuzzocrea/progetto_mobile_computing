#Orologio mondiale

Applicazione mobile cross-platform scritta con AngularJS e Apache Corova /Phonegap.

Quest'applicazione permette di visualizzare gli orari nelle diverse città del mondo e di effettuare conversioni di orario.